@extends('layouts.app')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <section class="section">
        <div class="section-header">
            <h1>History Laporan Kegiatan Anak Asuh PPA</h1>
        </div>

        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Latest Posts</h4>
                    <div class="card-header-action">
                        <a href="#" class="btn btn-primary">Edit</a>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th>Nama Anak Asuh</th>
                                    <th>Nama Koordinator</th>
                                    <th>Tingkat Sekolah</th>
                                    <th>Kelas</th>
                                    <th>Tulis hapalan untuk bulan ini (Nama Surah)</th>
                                    <th>Tuliskan ayat dari surat tersebut diatas (contoh: ayat 1 - 10)</th>
                                    <th>Apakah hapalan sudah di upload di youtube?</th>
                                    <th>Tuliskan link youtube</th>
                                    <th>Tanggal dibuat</th>
                                    <th style="min-width: 150px; text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($activity_ppa as $activity_ppa)
                                    <tr>
                                        <td>
                                            {{ $activity_ppa->nama_anak_asuh }}
                                        </td>
                                        <td>
                                            {{ $activity_ppa->nama_koordinator }}
                                        </td>
                                        <td>
                                            {{ $activity_ppa->tingkat_sekolah }}
                                        </td>
                                        <td>
                                            {{ $activity_ppa->kelas }}
                                        </td>
                                        <td>
                                            {{ $activity_ppa->nama_surah }}
                                        </td>
                                        <td>
                                            {{ $activity_ppa->jumlah_ayat }}
                                        </td>
                                        <td>
                                            {{ $activity_ppa->keterangan }}
                                        </td>
                                        <td>
                                            {{ $activity_ppa->link_youtube }}
                                        </td>
                                        <td>
                                            {{ $activity_ppa->created_at }}

                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-primary btn-action mr-1" data-toggle="tooltip"
                                                title="Edit"><i class="fas fa-pencil-alt"></i></a>

                                            <a class="btn btn-danger btn-action" href="#"
                                                onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                            <form id="delete-form"
                                                action="{{ route('activityPpa.delete', ['activity_ppa' => $activity_ppa]) }}"
                                                method="post" class="d-none">
                                                @csrf
                                                @method('delete')
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                        </td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
