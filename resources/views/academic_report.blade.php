@extends('layouts.app')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <section class="section">
        <div class="section-header">
            <h1>Laporan Akademik</h1>
        </div>
        <p>Lengkapi data dibawah ini sebelum submit !</p>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <form action="{{ route('store-academic') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group col-md-6">
                                        <label>Nama Anak Asuh</label>
                                        <input type="text" class="form-control" placeholder="Masukkan Nama"
                                            name="nama_anak_asuh">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>No. Telp wali/ anak asuh</label>
                                        <input type="text" class="form-control" placeholder="08123456789"
                                            name="no_telpon">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Tingkat Sekolah</label>
                                        <select class="form-control" name="tingkat_sekolah">
                                            <option selected disabled>Pilih Tingkat Sekolah</option>
                                            @foreach ($tingkat_sekolah as $tingkat_sekolah)
                                                <option>{{ $tingkat_sekolah }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Kelas</label>
                                        <select class="form-control" name="kelas">
                                            <option selected disabled>Pilih Kelas</option>
                                            @foreach ($kelas as $kelas)
                                                <option>{{ $kelas }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Semester</label>
                                        <select class="form-control" name="semester">
                                            <option selected disabled>Pilih Semester</option>
                                            @foreach ($semester as $semester)
                                                <option>{{ $semester }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Nilai Tertinggi (contoh: Matematika-87)</label>
                                        <input type="text" class="form-control" placeholder="Masukkan Nilai Tertinggi"
                                            name="nilai_tertinggi">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Nilai Terendah (contoh: Ipa-70)</label>
                                        <input type="text" class="form-control" placeholder="Masukkan Nama"
                                            name="nilai_terendah">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Nama Rata-rata (contoh : 82.5)</label>
                                        <input type="text" class="form-control" placeholder="Masukkan Nama"
                                            name="nilai_rata">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Foto Rapot</label>
                                        <input type="file" class="form-control" name="foto_rapot">
                                    </div>


                                    <div class="form-group col-md-12">
                                        <label class="d-block">Dengan ini saya menyatakan "Data yang telah saya isikan
                                            diatas benar adanya sesuai dengan kegiatan yang telah saya lakukan dan hanya
                                            Allah SWT sebagai saksinya "</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="keterangan"
                                                id="exampleRadios1" checked
                                                value="Saya mengisi data berdasarkan kegiatan saya">
                                            <label class="form-check-label" for="exampleRadios1">
                                                Saya mengisi data berdasarkan kegiatan saya
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="keterangan"
                                                id="exampleRadios2"
                                                value="Saya mengisi data tidak berdasarkan kegiatan saya">
                                            <label class="form-check-label" for="exampleRadios2">
                                                Saya mengisi data tidak berdasarkan kegiatan saya
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                <button class="btn btn-secondary" type="reset">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
