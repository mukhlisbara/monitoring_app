@extends('layouts.app')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <section class="section">
        <div class="section-header">
            <h1>Laporan Beasiswa</h1>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <form action="{{ route('update-beasiswa', $report->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="card">
                            <div class="card-body">
                                <div class="row">

                                    <div class="form-group col-md-6">
                                        <label>Nama Anak Asuh</label>
                                        <input type="text" value="{{ $report->nama_anak_asuh }}" class="form-control"
                                            name="nama_anak_asuh" placeholder="Masukkan Nama">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Nama Koordinator :</label>
                                        <select class="form-control" name="nama_koordinator">
                                            <option selected disabled>Pilih Nama Koordinator</option>

                                            @foreach ($nama_koordinator as $koor)
                                                <option {{ $report->nama_koordinator == $koor ? 'selected' : '' }}>
                                                    {{ $koor }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Foto Struk Beasiswa</label>
                                        <input type="file" class="form-control" name="foto_struk">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                <button class="btn btn-secondary" type="reset">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
