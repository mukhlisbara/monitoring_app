@extends('layouts.app')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif
    <section class="section">
        <div class="section-header">
            <h1>Laporan Kegiatan Anak Asuh PPA</h1>
        </div>
        <p>Lengkapi Data Di Bawah Ini Sebelum Submit !</p>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <form action="{{ route('store-activityPpa') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Nama Anak Asuh</label>
                                        <input type="text" class="form-control" name="nama_anak_asuh"
                                            placeholder="Masukkan Nama">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Nama Koordinator</label>
                                        <select class="form-control" name="nama_koordinator">
                                            <option selected disabled>Pilih Nama Koordinator</option>

                                            @foreach ($nama_koordinator as $koor)
                                                <option>{{ $koor }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Tingkat Sekolah</label>
                                        <select class="form-control" name="tingkat_sekolah">
                                            <option selected disabled>Pilih Tingkat Sekolah</option>
                                            @foreach ($tingkat_sekolah as $tingkat_sekolah)
                                                <option>{{ $tingkat_sekolah }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Kelas</label>
                                        <select class="form-control" name="kelas">
                                            <option selected disabled>Pilih Kelas</option>
                                            @foreach ($kelas as $kelas)
                                                <option>{{ $kelas }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Tulis hapalan untuk bulan ini (Nama Surah)</label>
                                        <input type="text" class="form-control" name="nama_surah"
                                            placeholder="Masukkan Hapalan Nama Surah">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Tuliskan ayat dari surat tersebut diatas (contoh: ayat 1 - 10)</label>
                                        <input type="text" class="form-control" name="jumlah_ayat"
                                            placeholder="Masukkan surat ">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Apakah hapalan sudah di upload di youtube? </label>
                                        <select class="form-control" name="link_youtube">
                                            <option selected disabled>Pilih Keterangan</option>
                                            @foreach ($keterangan as $keterangan)
                                                <option>{{ $keterangan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Tuliskan link youtube</label>
                                        <input type="text" class="form-control" name="keterangan"
                                            placeholder="Masukkan surat ">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                <button class="btn btn-secondary" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
