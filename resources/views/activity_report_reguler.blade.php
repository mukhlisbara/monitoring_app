@extends('layouts.app')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @elseif($message = Session::get('error'))
        <div class="alert alert-danger">
            <p>{{ $message }}</p>
        </div>
    @endif
    <section class="section">
        <div class="section-header">
            <h1>Laporan Kegiatan Anak Asuh Reguler</h1>
        </div>
        <p>Lengkapi Data Di Bawah Ini Sebelum Submit !</p>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <form action="{{ route('store-activityReguler') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Nama Koordinator</label>
                                        <select class="form-control" name="nama_koordinator">
                                            <option selected disabled>Pilih Nama Koordinator</option>

                                            @foreach ($nama_koordinator as $koor)
                                                <option>{{ $koor }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Nama Anak Asuh</label>
                                        <input type="text" class="form-control" placeholder="Masukkan Nama"
                                            name="nama_anak_asuh">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>No. Telp wali/ anak asuh</label>
                                        <input type="text" class="form-control" placeholder="08123456789"
                                            name="no_telpon">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Tingkat Sekolah</label>
                                        <select class="form-control" name="tingkat_sekolah">
                                            <option selected disabled>Pilih Tingkat Sekolah</option>
                                            @foreach ($tingkat_sekolah as $tingkat_sekolah)
                                                <option>{{ $tingkat_sekolah }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Kelas</label>
                                        <select class="form-control" name="kelas">
                                            <option selected disabled>Pilih Kelas</option>
                                            @foreach ($kelas as $kelas)
                                                <option>{{ $kelas }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Melaksanakan sholat 5 waktu</label>
                                        <select class="form-control" name="sholat_5waktu">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sholat 5 waktu di masjid</label>
                                        <select class="form-control" name="sholat_dimasjid">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sholat 5 waktu di awal waktu</label>
                                        <select class="form-control" name="sholat_diawal">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sholat sunnah rawatib</label>
                                        <select class="form-control" name="sholat_sunnah_rawatib">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sholat sunnah tahiyatul masjid</label>
                                        <select class="form-control" name="sholat_sunnah_tahiyatul">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sholat sunnah tahajud</label>
                                        <select class="form-control" name="sholat_sunnah_tahajud">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sholat sunnah dhuha</label>
                                        <select class="form-control" name="sholat_sunnah_dhuha">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Sholat sunnah fajar</label>
                                        <select class="form-control" name="sholat_sunnah_fajar">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="form-group col-md-6">
                                        <label>Sholat sunnah wudhu</label>
                                        <select class="form-control" name="sholat_sunnah_wudhu">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div> --}}
                                    <div class="form-group col-md-6">
                                        <label>Sholat sunnah hajad</label>
                                        <select class="form-control" name="sholat_sunnah_hajad">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Membaca Al Qur'an</label>
                                        <select class="form-control" name="membaca_alquran">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Melakukan hafalan Al Qur'an</label>
                                        <select class="form-control" name="hafalan_alquran">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Puasa Ramadhan (tidak perlu diisi jika bukan bulan Ramadhan)</label>
                                        <select class="form-control" name="puasa_ramadhan">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Puasa sunnah senin-kamis atau puasa sunnah yang lain</label>
                                        <select class="form-control" name="puasa_sunnah">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Melakukan infaq atau sedekah</label>
                                        <select class="form-control" name="infaq_sedekah">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Membantu orang tua di rumah</label>
                                        <select class="form-control" name="membantu_orangtua">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Memiliki hubungan yang baik dengan orang tua atau wali</label>
                                        <select class="form-control" name="hubungan_ortu_wali">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Belajar mandiri di rumah</label>
                                        <select class="form-control" name="belajar_mandiri">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Belajar berkelompok dengan teman</label>
                                        <select class="form-control" name="belajar_kelompok">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Membantu teman atau orang lain yang kesulitan</label>
                                        <select class="form-control" name="membantu_teman">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Belajar mengaji dengan pembimbing guru/ ustadz</label>
                                        <select class="form-control" name="mengaji_ustadz">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Berapa jumlah hafalan Al Qur'an dalam bulan ini</label>
                                        <select class="form-control" name="jumlah_hafalan">
                                            <option selected disabled>Pilih Kegiatan</option>
                                            @foreach ($kegiatans as $kegiatan)
                                                <option>{{ $kegiatan }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label class="d-block">Dengan ini saya menyatakan "Data yang telah saya isikan
                                            diatas benar adanya sesuai dengan kegiatan yang telah saya lakukan dan hanya
                                            Allah SWT sebagai saksinya "</label>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="keterangan"
                                                id="exampleRadios1" checked>
                                            <label class="form-check-label" for="exampleRadios1">
                                                Saya mengisi data berdasarkan kegiatan saya
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="keterangan"
                                                id="exampleRadios2">
                                            <label class="form-check-label" for="exampleRadios2">
                                                Saya mengisi data tidak berdasarkan kegiatan saya
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                <button class="btn btn-secondary" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
