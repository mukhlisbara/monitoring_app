<!DOCTYPE html>

@include('layouts.style')

<body>
    <div id="app">
        <div class="main-wrapper">


            <!-- Main Content -->

            @yield('content')

        </div>
    </div>


</body>

</html>
