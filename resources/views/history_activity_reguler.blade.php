@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>History Laporan Kegiatan Anak Asuh reguler</h1>
        </div>

        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Latest Posts</h4>
                    <div class="card-header-action">
                        <a href="#" class="btn btn-primary">Edit</a>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th>Nama Anak Asuh</th>
                                    <th>Nama Koordinator</th>
                                    <th>No. Telp wali/ anak asuh</th>
                                    <th>Tingkat Sekolah</th>
                                    <th>Kelas</th>
                                    <th>Melaksanakan sholat 5 waktu</th>
                                    <th>Sholat 5 waktu di masjid</th>
                                    <th>Sholat 5 waktu di awal waktu</th>
                                    <th>Sholat sunnah rawatib</th>
                                    <th>Sholat sunnah tahiyatul masjid</th>
                                    <th>Sholat sunnah tahajud</th>
                                    <th>Sholat sunnah dhuha</th>
                                    <th>Sholat sunnah fajar</th>
                                    <th>Sholat sunnah wudhu</th>
                                    <th>Sholat sunnah hajad</th>
                                    <th>Membaca Al Qur'an</th>
                                    <th>Melakukan hafalan Al Qur'an</th>
                                    <th>Puasa Ramadhan</th>
                                    <th>Puasa sunnah senin-kamis atau puasa sunnah yang lain</th>
                                    <th>Melakukan infaq atau sedekah</th>
                                    <th>Membantu orang tua di rumah</th>
                                    <th>Memiliki hubungan yang baik dengan orang tua atau wali</th>
                                    <th>Belajar mandiri di rumah</th>
                                    <th>Belajar berkelompok dengan teman</th>
                                    <th>Membantu teman atau orang lain yang kesulitan</th>
                                    <th>Belajar mengaji dengan pembimbing guru/ ustadz</th>
                                    <th>Berapa jumlah hafalan Al Qur'an dalam bulan ini</th>
                                    <th>Pernyataan</th>
                                    <th>Tanggal dibuat</th>
                                    <th style="min-width: 150px; text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($activity_reguler as $activity_reguler)
                                    <tr>
                                        <td>
                                            {{ $activity_reguler->nama_anak_asuh }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->nama_koordinator }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->no_telpon }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->tingkat_sekolah }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->kelas }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_5waktu }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_dimasjid }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_diawal }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_sunnah_rawatib }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_sunnah_tahiyatul }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_sunnah_tahajud }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_sunnah_dhuha }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_sunnah_fajar }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_sunnah_wudhu }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->sholat_sunnah_hajad }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->membaca_alquran }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->hafalan_alquran }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->puasa_ramadhan }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->puasa_sunnah }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->infaq_sedekah }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->membantu_orangtua }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->hubungan_ortu_wali }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->belajar_mandiri }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->belajar_kelompok }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->membantu_teman }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->mengaji_ustadz }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->jumlah_hafalan }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->keterangan }}
                                        </td>
                                        <td>
                                            {{ $activity_reguler->created_at }}

                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-primary btn-action mr-1" data-toggle="tooltip"
                                                title="Detail"><i class="fas fa-eye"></i></a>

                                            <a class="btn btn-danger btn-action" href="#"
                                                onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                            <form id="delete-form"
                                                action="{{ route('activityReguler.delete', $activity_reguler) }}"
                                                method="post" class="d-none">
                                                @csrf
                                                @method('delete')
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                        </td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
