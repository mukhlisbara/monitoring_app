@extends('layouts.app')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <section class="section">
        <div class="section-header">
            <h1>History Laporan Beasiswa</h1>
        </div>

        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Latest Posts</h4>
                    <div class="card-header-action">
                        <a href="#" class="btn btn-primary">Edit</a>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th>Nama Anak Asuh</th>
                                    <th>Nama Koordinator</th>
                                    <th>Foto Struk Beasiswa</th>
                                    <th>Tanggal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($beasiswas as $beasiswa)
                                    <tr>
                                        <td>
                                            {{ $beasiswa->nama_anak_asuh }}
                                        </td>
                                        <td>
                                            {{ $beasiswa->nama_koordinator }}
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)"
                                                data-fancybox="preview-thumbnail-{{ $beasiswa->id }}"
                                                data-src="{{ asset("assets/img/struk/$beasiswa->foto_struk") }}">
                                                <img src="{{ asset("assets/img/struk/$beasiswa->foto_struk") }}"
                                                    alt="" style="height:100px">
                                            </a>
                                        </td>
                                        <td>
                                            {{ $beasiswa->created_at }}

                                        </td>
                                        <td>
                                            <a href="{{ route('beasiswa_report.edit', $beasiswa->id) }}"
                                                class="btn btn-primary btn-action mr-1" data-toggle="tooltip"
                                                title="Edit"><i class="fas fa-pencil-alt"></i></a>

                                            <a class="btn btn-danger btn-action" href="#"
                                                onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                            <form id="delete-form"
                                                action="{{ route('beasiswaReport.delete', ['beasiswa' => $beasiswa]) }}"
                                                method="post" class="d-none">
                                                @csrf
                                                @method('delete')
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                        </td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        // Start SWAL when delete message btn clicked
        $('body').on('click', '.delete-msg', function() {
            let id = $(this).data('id');
            console.log($(this).data('id'));
            Swal.fire({
                title: 'Are you sure',
                text: "Want to delete this chat? Deleted data cannot be recovered!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Delete It!'
            }).then((result) => {
                if (result.isConfirmed) {
                    console.log(id);
                    @this.deleteChat(id);
                }
            });
        });
        // End SWAL when delete message btn clicked
    </script>
@endsection
