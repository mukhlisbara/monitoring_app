@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>History Laporan Akademik</h1>
        </div>

        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Latest Posts</h4>
                    <div class="card-header-action">
                        <a href="#" class="btn btn-primary">Edit</a>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th>Nama Anak Asuh</th>
                                    <th>No. Telp wali/ anak asuh</th>
                                    <th>Tingkat Sekolah</th>
                                    <th>Kelas</th>
                                    <th>Semester</th>
                                    <th>Nilai Tertinggi</th>
                                    <th>Nilai Terendah</th>
                                    <th>Nilai Rata-rata</th>
                                    <th>Foto Rapot</th>
                                    <th>Tanggal dibuat</th>
                                    <th>Keterangan</th>
                                    <th style="min-width: 150px; text-align: center;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($academic_report as $academic_report)
                                    <tr>
                                        <td>
                                            {{ $academic_report->nama_anak_asuh }}
                                        </td>
                                        <td>
                                            {{ $academic_report->no_telpon }}
                                        </td>
                                        <td>
                                            {{ $academic_report->tingkat_sekolah }}
                                        </td>
                                        <td>
                                            {{ $academic_report->kelas }}
                                        </td>
                                        <td>
                                            {{ $academic_report->semester }}
                                        </td>
                                        <td>
                                            {{ $academic_report->nilai_tertinggi }}
                                        </td>
                                        <td>
                                            {{ $academic_report->nilai_terendah }}
                                        </td>
                                        <td>
                                            {{ $academic_report->nilai_rata }}
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)"
                                                data-fancybox="preview-thumbnail-{{ $academic_report->id }}"
                                                data-src="{{ asset("assets/img/rapot/$academic_report->foto_rapot") }}">
                                                <img src="{{ asset("assets/img/rapot/$academic_report->foto_rapot") }}"
                                                    alt="" style="height:100px">
                                            </a>
                                        </td>
                                        <td>
                                            {{ $academic_report->created_at }}
                                        </td>
                                        <td>
                                            {{ $academic_report->keterangan }}
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-primary btn-action mr-1" data-toggle="tooltip"
                                                title="Edit"><i class="fas fa-pencil-alt"></i></a>
                                            <a class="btn btn-danger btn-action" href="#"
                                                onclick="event.preventDefault();
                                                     document.getElementById('delete-form').submit();">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                            <form id="delete-form"
                                                action="{{ route('academic.delete', $academic_report) }}" method="post"
                                                class="d-none">
                                                @csrf
                                                @method('delete')
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" class="text-center">
                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                        </td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
