@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Daftar Pengajuan Beasiswa</h1>

        </div>
        @forelse ($pengajuan_beasiswas as $pengajuan_beasiswa)
            <div class="section-body">

                <div class="card card-body">

                    <div class="row mt-sm-4">
                        <div class="col-12 col-md-12 col-lg-3">
                            <div class="card border">
                                <form method="post" class="needs-validation" novalidate="">
                                    <div class="card-header">
                                        <h4>Foto Pribadi</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-10 col-lg-10 col-md-6 col-sm-10 col-10 px-5 px-lg-0 px-md-2">
                                                {{-- <img class="img-thumbnail responsive"
                                                    src="{{ asset('assets/img/' . $pengajuan_beasiswa->foto_anak_asuh) }}"
                                                    style="" alt="image"> --}}
                                                <a href="javascript:void(0)"
                                                    data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                    data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_anak_asuh") }}">
                                                    <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_anak_asuh") }}"
                                                        alt="" style="" class="img-thumbnail responsive">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-9">
                            <div class="card border">
                                <form method="post" class="needs-validation" novalidate="">
                                    <div class="card-body">
                                        <table class="table">
                                            {{-- <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">First</th>
                                                <th scope="col">Last</th>
                                                <th scope="col">Handle</th>
                                            </tr>
                                        </thead> --}}

                                            <tbody>

                                                <tr class="table-secondary">
                                                    <th scope="row">Nama</th>
                                                    <td>
                                                        {{ $pengajuan_beasiswa->nama ? $pengajuan_beasiswa->nama : '-' }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Jenis Kelamin</th>
                                                    <td>{{ $pengajuan_beasiswa->gender ? $pengajuan_beasiswa->gender : '-' }}
                                                    </td>
                                                </tr>
                                                <tr class="table-secondary">
                                                    <th scope="row">Tempat dan tanggal lahir</th>
                                                    <td colspan="2">
                                                        {{ $pengajuan_beasiswa->ttl ? $pengajuan_beasiswa->ttl : '-' }}</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Status Anak Asuh</th>
                                                    <td colspan="2">
                                                        {{ $pengajuan_beasiswa->status_anak_asuh ? $pengajuan_beasiswa->status_anak_asuh : '-' }}
                                                    </td>
                                                </tr>
                                                <tr class="table-secondary">
                                                    <th scope="row">Tingkat Sekolah</th>
                                                    <td colspan="2">
                                                        {{ $pengajuan_beasiswa->tingkat_sekolah ? $pengajuan_beasiswa->tingkat_sekolah : '-' }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Bulan Juli tahun ini kelas berapa</th>
                                                    <td colspan="2">
                                                        {{ $pengajuan_beasiswa->kelas_juli_ini ? $pengajuan_beasiswa->kelas_juli_ini : '-' }}
                                                    </td>
                                                </tr>
                                                <tr class="table-secondary">
                                                    <th scope="row">Nama Sekolah</th>
                                                    <td colspan="2">
                                                        {{ $pengajuan_beasiswa->nama_sekolah ? $pengajuan_beasiswa->nama_sekolah : '-' }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Alamat Sekolah</th>
                                                    <td colspan="2">
                                                        {{ $pengajuan_beasiswa->alamat_sekolah ? $pengajuan_beasiswa->alamat_sekolah : '-' }}
                                                    </td>
                                                </tr>
                                                <tr class="table-secondary">
                                                    <th scope="row">Hubungan wali dengan anak yang diajukan</th>
                                                    <td colspan="2">
                                                        {{ $pengajuan_beasiswa->hubungan_wali_dengan_anak ? $pengajuan_beasiswa->hubungan_wali_dengan_anak : '-' }}
                                                    </td>
                                                </tr>



                                            </tbody>

                                        </table>

                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        @empty
            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-3">
                    <div class="card border">
                        <form method="post" class="needs-validation" novalidate="">
                            <div class="card-header">
                                <h4>Foto Pribadi</h4>
                            </div>
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-xl-10 col-lg-10 col-md-6 col-sm-10 col-10 px-5 px-lg-0 px-md-2">
                                        <img class="img-thumbnail responsive" src="{{ asset('assets/img/p3.jpg') }}"
                                            style="" alt="image">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-9">
                    <div class="card border">
                        <form method="post" class="needs-validation" novalidate="">
                            <div class="card-body">
                                <table class="table">
                                    <tbody>

                                        <tr>
                                            <th scope="row">1</th>
                                            <td>
                                                -
                                            </td>
                                            <td>-</td>
                                        </tr>
                                        <tr class="table-secondary">
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>

                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td colspan="2">Larry the Bird</td>

                                        </tr>

                                    </tbody>

                                </table>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforelse
    </section>
@endsection
