<!DOCTYPE html>

@include('layouts.style')

<body>
    <div id="app">
        <div class="main-wrapper">
            @include('layouts.navbar')
            @include('layouts.sidebar')

            <!-- Main Content -->
            <div class="main-content">
                @yield('content')
            </div>
        </div>
    </div>


</body>

</html>
