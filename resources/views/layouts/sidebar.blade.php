<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">Peduli Yatim PENS</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <img src="../assets/img/logoyatim-min.png" alt="" style="max-width: 35%">
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            {{-- <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li class="active"><a class="nav-link" href="index-0.html">General Dashboard</a></li>
                    <li><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
                </ul>
            </li> --}}
            <li class="{{ Route::is('home') ? 'active' : '' }}"><a class="nav-link" href="{{ route('home') }}">
                    <i class="fas fa-chart-line"></i><span>Dashboard</span></a></li>

            <li class="menu-header">Input Laporan</li>
            <li class="{{ Route::is('pengajuan_beasiswa') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('pengajuan_beasiswa') }}">
                    <i class="fas fa-handshake"></i> <span>Pengajuan Beasiswa</span></a></li>
            <li class="{{ Route::is('activity_report_reguler') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('activity_report_reguler') }}">
                    <i class="fas fa-book"></i> <span>Laporan Kegiatan</span></a></li>
            <li class="{{ Route::is('academic_report') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('academic_report') }}">
                    <i class="fas fa-graduation-cap"></i><span>Laporan Akademik</span></a></li>
            <li class="{{ Route::is('activity_report_ppa') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('activity_report_ppa') }}">
                    <i class="fas fa-book-quran"></i><span>Laporan Kegiatan</span></a></li>
            <li class="{{ Route::is('beasiswa_report') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('beasiswa_report') }}">
                    <i class="fas fa-receipt"></i><span>Laporan Beasiswa</span></a></li>

            <li class="menu-header">Riwayat History</li>
            <li class="{{ Route::is('history_pengajuan_beasiswa') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('history_pengajuan_beasiswa') }}">
                    <i class="fas fa-clock-rotate-left"></i> <span>Riwayat Pengajuan</span></a></li>
            <li class="{{ Route::is('history_activity_reguler') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('history_activity_reguler') }}">
                    <i class="fas fa-clock-rotate-left"></i> <span>Riwayat Kegiatan</span></a></li>
            <li class="{{ Route::is('history_academic_report') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('history_academic_report') }}">
                    <i class="fas fa-clock-rotate-left"></i><span>Riwayat Akademik</span></a></li>
            <li class="{{ Route::is('history_activity_ppa') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('history_activity_ppa') }}">
                    <i class="fas fa-clock-rotate-left"></i><span>Riwayat Kegiatan</span></a></li>
            <li class="{{ Route::is('history_beasiswa_report') ? 'active' : '' }}"><a class="nav-link"
                    href="{{ route('history_beasiswa_report') }}">
                    <i class="fas fa-clock-rotate-left"></i><span>Riwayat Beasiswa</span></a></li>
        </ul>
    </aside>
</div>
