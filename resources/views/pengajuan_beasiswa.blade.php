@extends('layouts.app')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <section class="section">
        <div class="section-header">
            <h1>Pengajuan Beasiswa</h1>
        </div>
        <p>Lengkapi Data Di Bawah Ini Sebelum Submit !</p>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <form action="{{ route('pengajuan_beasiswa.store') }}" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="card-body">

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-orangtua-tab" data-toggle="pill"
                                            href="#pills-orangtua" role="tab" aria-controls="pills-orangtua"
                                            aria-selected="true">Data Orang Tua /
                                            Wali</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-anakasuh-tab" data-toggle="pill"
                                            href="#pills-anakasuh" role="tab" aria-controls="pills-anakasuh"
                                            aria-selected="false">Data Anak
                                            Asuh</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-dokumen-tab" data-toggle="pill" href="#pills-dokumen"
                                            role="tab" aria-controls="pills-dokumen" aria-selected="false">Dokumen</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-pernyataan-tab" data-toggle="pill"
                                            href="#pills-pernyataan" role="tab" aria-controls="pills-pernyataan"
                                            aria-selected="false">Pernyataan</a>
                                    </li>
                                </ul>

                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-orangtua" role="tabpanel"
                                        aria-labelledby="pills-orangtua-tab">
                                        <div class="row">
                                            {{-- <div class="form-group col-md-6">
                                                <label>Tipe Pendaftaran :</label>
                                                <select class="form-control" name="tipe_pendaftaran">
                                                    <option selected disabled>Pilih Tipe Pendaftaran</option>

                                                    @foreach ($beasiswa as $beasiswa)
                                                        <option>{{ $beasiswa }}</option>
                                                    @endforeach
                                                </select>
                                            </div> --}}
                                            <div class="form-group col-md-6">
                                                <label>Nama Koordinator :</label>
                                                <select class="form-control" name="nama_koordinator">
                                                    <option selected disabled>Pilih Nama Koordinator</option>

                                                    @foreach ($nama_koordinator as $koor)
                                                        <option>{{ $koor }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Nama orang tua/ wali :</label>
                                                <input type="text" name="nama_ortu" class="form-control"
                                                    placeholder="Masukkan Nama Orang Tua">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tempat lahir :</label>
                                                <input name="ttl_ortu" type="text" class="form-control"
                                                    placeholder="Masukkan Tempat Lahir">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tanggal lahir :</label>
                                                <input type="date" class="form-control"
                                                    placeholder="Masukkan Tanggal Lahir">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Status Perkawinan :</label>
                                                <input name="status_kawin" type="text" class="form-control"
                                                    placeholder="Masukkan Status Perkawinan">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pendidikan terakhir :</label>
                                                <input name="pendidikan_terakhir" type="text" class="form-control"
                                                    placeholder="Pendidikan Terakhir">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Alamat (Jalan, No rumah, Desa, Kecamatan) :</label>
                                                <input name="alamat_detail" type="text" class="form-control"
                                                    placeholder="Masukkan Alamat (Jalan)">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Alamat (Kota):</label>
                                                <input name="kota" type="text" class="form-control"
                                                    placeholder="Masukkan Alamat (Kota)">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pekerjaan :</label>
                                                <input name="pekerjaan" type="text" class="form-control"
                                                    placeholder="Masukkan Pekerjaan">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Pendapatan rata-rata perbulan (Rupiah):</label>
                                                <input name="pendapatan" type="text" class="form-control"
                                                    placeholder="Rp.,-">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Rumah yang ditempati adalah:</label>
                                                <input name="hak_kepemilikan_rumah" type="text" class="form-control"
                                                    placeholder="Masukkan Alamat Yang Ditempati">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Jumlah jiwa yang ditanggung (tidak termasuk diri sendiri):</label>
                                                <input name="jiwa_ditanggung" type="text" class="form-control"
                                                    placeholder="Masukkan jumlah jiwa yang ditanggung">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Kategori jiwa yang ditanggung meliputi:</label>
                                                <input name="siapa_ditanggung" type="text" class="form-control"
                                                    placeholder="Masukkan Kategori">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-anakasuh" role="tabpanel"
                                        aria-labelledby="pills-anakasuh-tab">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Nama :</label>
                                                <input name="nama" type="text" class="form-control"
                                                    placeholder="Masukkan Nama">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Jenis Kelamin</label>
                                                <select name="gender" class="form-control">
                                                    <option selected disabled>Pilih Jenis Kelamin</option>

                                                    @foreach ($kelamin as $kelamin)
                                                        <option>{{ $kelamin }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label>Tempat lahir :</label>
                                                <input name="ttl" type="text" class="form-control"
                                                    placeholder="Masukkan Tempat Lahir">
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label>Tanggal lahir :</label>
                                                <input type="date" class="form-control"
                                                    placeholder="Masukkan Tanggal Lahir">
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label>Status Anak Asuh :</label>
                                                <input name="status_anak_asuh" type="text" class="form-control"
                                                    placeholder="Masukkan Status Anak Asuh">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tingkat Sekolah</label>
                                                <select name="tingkat_sekolah" class="form-control">
                                                    <option selected disabled>Pilih Tingkat Sekolah</option>

                                                    @foreach ($tingkat_sekolah as $tingkat_sekolah)
                                                        <option>{{ $tingkat_sekolah }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Bulan Juli tahun ini kelas berapa :</label>
                                                <select name="kelas_juli_ini" class="form-control">
                                                    <option selected disabled>Pilih Kelas</option>

                                                    @foreach ($kelas as $kelas)
                                                        <option>{{ $kelas }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Nama Sekolah :</label>
                                                <input name="nama_sekolah" type="text" class="form-control"
                                                    placeholder="Masukkan Nama Sekolah">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Alamat (Jalan, No rumah, Desa, Kecamatan) :</label>
                                                <input type="text" class="form-control"
                                                    placeholder="Masukkan Alamat (Jalan, No rumah, Desa, Kecamatan)">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Alamat (Kota):</label>
                                                <input type="text" class="form-control"
                                                    placeholder="Masukkan Alamat (Kota) ">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Hubungan wali dengan anak yang diajukan:</label>
                                                <input name="hubungan_wali_dengan_anak" type="text"
                                                    class="form-control" placeholder="Masukkan Hubungan Wali dengan Anak">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Kirim Foto Anak Asuh:</label>
                                                <input name="foto_anak_asuh" type="file" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-dokumen" role="tabpanel"
                                        aria-labelledby="pills-dokumen-tab">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label>Kirim file KTP orang tua/ wali:</label>
                                                <input name="ktp_ortu" type="file" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Kirim file KK:</label>
                                                <input name="kk" type="file" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Kirim surat keterangan masih sekolah atau bukti rapor terakhir
                                                    :</label>
                                                <input name="bukti_pernah_sekolah" type="file" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Kirim foto rumah tampak depan
                                                </label>
                                                <input name="foto_rumah_depan" type="file" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Kirim foto ruang tamu
                                                </label>
                                                <input name="foto_ruang_tamu" type="file" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Kirim foto dapur
                                                </label>
                                                <input name="foto_dapur" type="file" class="form-control">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Surat keterangan warga tidak mampu, silakan print diisi sesuai dengan
                                                    format surat :</label>
                                                <input name="sktm" type="file" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-pernyataan" role="tabpanel"
                                        aria-labelledby="pills-pernyataan-tab">
                                        <div class="row">
                                            {{-- <div class="form-group col-md-6">
                                                <label>Koordinator menanyakan ke orang tua/ wali anak asuh -->
                                                    <span style="color:red">
                                                        Saya selaku orang tua/ wali anak asuh yang namanya telah terdaftar
                                                        diatas, bersedia dan
                                                        sanggup untuk membina anak asuh, menyangkut pembinaan agama, akhlaq
                                                        ,
                                                        moral,
                                                        akademi, ketrampilan serta kemampuan anak asuh.
                                                    </span>
                                                </label>
                                                <input name="foto_ruang_tamu" type="text" class="form-control"
                                                    placeholder="Masukkan Nama">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Koordinator menanyakan ke orang tua/ wali anak asuh -->
                                                    <span style="color:red">
                                                        Saya sebagai orang tua/ wali dari anak asuh yang diajukan, dengan
                                                        ini
                                                        menyatakan bahwa DATA YANG TELAH SAYA ISIKAN DIATAS ADALAH BENAR.
                                                        Dan
                                                        jika ternyata saya dengan sengaja berbohong atas data yang telah
                                                        diisikan diatas, saya bersedia menanggung akibatnya termasuk
                                                        tuntutan
                                                        hukum.
                                                    </span>
                                                </label>
                                                <input name="foto_dapur" type="text" class="form-control"
                                                    placeholder="Masukkan Nama">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Nama Orang Tua/ Wali :</label>
                                                <input name="no_ktp" type="text" class="form-control"
                                                    placeholder="Masukkan Nama">
                                            </div> --}}
                                            <div class="form-group col-md-6">
                                                <label>Nomor KTP :</label>
                                                <input name="no_ktp" type="text" class="form-control"
                                                    placeholder="Masukkan No. KTP">
                                            </div>
                                            {{-- <div class="form-group col-md-6">
                                                <label>Saya selaku koordinator telah melakukan evaluasi pengajuan ini dan
                                                    memastikan antara data yang diisi dan kondisi yang sebenarnya
                                                    adalah :</label>
                                                <input name="lokasi_pengajuan" type="text" class="form-control">
                                            </div> --}}
                                            <div class="form-group col-md-6">
                                                <label>Tempat/ Kota mengisi Formulir :</label>
                                                <input name="lokasi_pengajuan" type="text" class="form-control"
                                                    placeholder="Masukkan Nama Kota">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Tanggal mengisi Formulir</label>
                                                <input name="tanggal_pengajuan" type="date" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                {{-- <button class="btn btn-secondary" type="reset">Reset</button> --}}
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
