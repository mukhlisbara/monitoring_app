@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>History Pengajuan Beasiswa</h1>

        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <form action="{{ route('pengajuan_beasiswa.store') }}" enctype="multipart/form-data" method="post">
                            @csrf
                            <div class="card-body">

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-orangtua-tab" data-toggle="pill"
                                            href="#pills-orangtua" role="tab" aria-controls="pills-orangtua"
                                            aria-selected="true">Data Orang Tua /
                                            Wali</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-anakasuh-tab" data-toggle="pill"
                                            href="#pills-anakasuh" role="tab" aria-controls="pills-anakasuh"
                                            aria-selected="false">Data Anak
                                            Asuh</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-dokumen-tab" data-toggle="pill" href="#pills-dokumen"
                                            role="tab" aria-controls="pills-dokumen" aria-selected="false">Dokumen</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-pernyataan-tab" data-toggle="pill"
                                            href="#pills-pernyataan" role="tab" aria-controls="pills-pernyataan"
                                            aria-selected="false">Pernyataan</a>
                                    </li>
                                </ul>

                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-orangtua" role="tabpanel"
                                        aria-labelledby="pills-orangtua-tab">
                                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Latest Posts</h4>
                                                    <div class="card-header-action">
                                                        <a href="#" class="btn btn-primary">Edit</a>
                                                    </div>
                                                </div>
                                                <div class="card-body p-0">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-0">
                                                            <thead>
                                                                <tr>
                                                                    {{-- <th>Tipe Pendaftaran</th> --}}
                                                                    <th>Nama Koordinator</th>
                                                                    <th>Nama orang tua/ wali</th>
                                                                    <th>Tempat dan tanggal lahir</th>
                                                                    <th>Status Perkawinan</th>
                                                                    <th>Pendidikan Terakhir</th>
                                                                    <th>Alamat (Jalan, No rumah, Desa, Kecamatan)</th>
                                                                    <th>Alamat (Kota)</th>
                                                                    <th>Pekerjaan</th>
                                                                    <th>Pendapatan rata-rata perbulan (Rupiah)</th>
                                                                    <th>Rumah yang ditempati adalah</th>
                                                                    <th>Jumlah jiwa yang ditanggung (tidak termasuk diri
                                                                        sendiri)</th>
                                                                    <th>Kategori jiwa yang ditanggung meliputi</th>
                                                                    <th>Tanggal Diubah</th>
                                                                    <th style="min-width: 150px; text-align: center;">Action
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse ($pengajuan_beasiswas as $pengajuan_beasiswa)
                                                                    <tr>
                                                                        {{-- <td>
                                                                            {{ $pengajuan_beasiswa->tipe_pendaftaran }}
                                                                        </td> --}}
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->nama_koordinator }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->nama_ortu }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->ttl_ortu }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->status_kawin }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->pendidikan_terakhir }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->kota }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->alamat_detail }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->pekerjaan }}
                                                                        </td>

                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->pendapatan }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->hak_kepemilikan_rumah }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->jiwa_ditanggung }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->siapa_ditanggung }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->created_at }}
                                                                        </td>
                                                                        <td>
                                                                            <a href="#"
                                                                                class="btn btn-primary btn-action mr-1"
                                                                                data-toggle="tooltip" title="Edit"><i
                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                            <a href="#"
                                                                                class="btn btn-danger btn-action"
                                                                                onclick="event.preventDefault(); 
                                                                                document.getElementById('delete-form').submit();">
                                                                                <i class="fas fa-trash"></i>
                                                                            </a>
                                                                            <form id="delete-form"
                                                                                action="{{ route('pengajuan_beasiswa.delete', $pengajuan_beasiswa) }}"
                                                                                method="post" class="d-none">
                                                                                @csrf
                                                                                @method('delete')
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                    <tr>
                                                                        <td colspan="4" class="text-center">
                                                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                                                        </td>
                                                                    </tr>
                                                                @endforelse

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="tab-pane fade" id="pills-anakasuh" role="tabpanel"
                                        aria-labelledby="pills-anakasuh-tab">
                                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Latest Posts</h4>
                                                    <div class="card-header-action">
                                                        <a href="#" class="btn btn-primary">Edit</a>
                                                    </div>
                                                </div>
                                                <div class="card-body p-0">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-0">
                                                            <thead>
                                                                <tr>
                                                                    <th>nama</th>
                                                                    <th>Jenis Kelamin</th>
                                                                    <th>Tempat dan tanggal lahir</th>
                                                                    <th>Status Anak Asuh</th>
                                                                    <th>Tingkat Sekolah</th>
                                                                    <th>Bulan Juli tahun ini kelas berapa</th>
                                                                    <th>Nama Sekolah</th>
                                                                    <th>Alamat Sekolah</th>
                                                                    <th>Hubungan wali dengan anak yang diajukan</th>
                                                                    <th>Kirim Foto Anak Asuh</th>
                                                                    <th>Tanggal Diubah</th>
                                                                    <th style="min-width: 150px; text-align: center;">Action
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse ($pengajuan_beasiswas as $pengajuan_beasiswa)
                                                                    <tr>
                                                                        {{-- @dd($pengajuan_beasiswa->nama); --}}
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->nama }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->gender }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->ttl }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->status_anak_asuh }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->tingkat_sekolah }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->kelas_juli_ini }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->nama_sekolah }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->alamat_sekolah }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->hubungan_wali_dengan_anak }}
                                                                        </td>

                                                                        {{-- <td>
                                                                            {{ $pengajuan_beasiswa->foto_anak_asuh }}
                                                                        </td> --}}

                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                                                data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_anak_asuh") }}">
                                                                                <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_anak_asuh") }}"
                                                                                    alt="" style="height:100px">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->created_at }}
                                                                        </td>
                                                                        <td>
                                                                            <a href="#"
                                                                                class="btn btn-primary btn-action mr-1"
                                                                                data-toggle="tooltip" title="Edit"><i
                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                            <a href="#"
                                                                                class="btn btn-danger btn-action"
                                                                                onclick="event.preventDefault(); 
                                                                                document.getElementById('delete-form').submit();">
                                                                                <i class="fas fa-trash"></i>
                                                                            </a>
                                                                            <form id="delete-form"
                                                                                action="{{ route('pengajuan_beasiswa.delete', $pengajuan_beasiswa) }}"
                                                                                method="post" class="d-none">
                                                                                @csrf
                                                                                @method('delete')
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                    <tr>
                                                                        <td colspan="4" class="text-center">
                                                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                                                        </td>
                                                                    </tr>
                                                                @endforelse

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-dokumen" role="tabpanel"
                                        aria-labelledby="pills-dokumen-tab">
                                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Latest Posts</h4>
                                                    <div class="card-header-action">
                                                        <a href="#" class="btn btn-primary">Edit</a>
                                                    </div>
                                                </div>
                                                <div class="card-body p-0">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-0">
                                                            <thead>
                                                                <tr>
                                                                    <th>Kirim file KTP orang tua/ wali</th>
                                                                    <th>Kirim file KK</th>
                                                                    <th>Kirim surat keterangan masih sekolah atau bukti
                                                                        rapor terakhir</th>
                                                                    <th>Surat keterangan warga tidak mampu, silakan print
                                                                        diisi sesuai dengan format surat </th>
                                                                    <th>Kirim foto rumah tampak depan</th>
                                                                    <th>Kirim foto ruang tamu</th>
                                                                    <th>Kirim foto dapur</th>
                                                                    <th>Nomor KTP</th>
                                                                    <th>Lokasi Pengajuan</th>
                                                                    <th>Tanggal Pengajuan</th>
                                                                    <th>Tanggal Diubah</th>
                                                                    <th style="min-width: 150px; text-align: center;">
                                                                        Action
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse ($pengajuan_beasiswas as $pengajuan_beasiswa)
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                                                data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->ktp_ortu") }}">
                                                                                <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->ktp_ortu") }}"
                                                                                    alt="" style="height:100px">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                                                data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->kk") }}">
                                                                                <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->kk") }}"
                                                                                    alt="" style="height:100px">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                                                data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->bukti_pernah_sekolah") }}">
                                                                                <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->bukti_pernah_sekolah") }}"
                                                                                    alt="" style="height:100px">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                                                data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->sktm") }}">
                                                                                <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->sktm") }}"
                                                                                    alt="" style="height:100px">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                                                data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_rumah_depan") }}">
                                                                                <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_rumah_depan") }}"
                                                                                    alt="" style="height:100px">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                                                data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_ruang_tamu") }}">
                                                                                <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_ruang_tamu") }}"
                                                                                    alt="" style="height:100px">
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="javascript:void(0)"
                                                                                data-fancybox="preview-thumbnail-{{ $pengajuan_beasiswa->id }}"
                                                                                data-src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_dapur") }}">
                                                                                <img src="{{ asset("assets/img/pengajuan/$pengajuan_beasiswa->foto_dapur") }}"
                                                                                    alt="" style="height:100px">
                                                                            </a>
                                                                        </td>

                                                                        {{-- <td>
                                                                            {{ $pengajuan_beasiswa->ktp_ortu }}
                                                                        </td> --}}
                                                                        {{-- <td>
                                                                            {{ $pengajuan_beasiswa->kk }}
                                                                        </td> --}}
                                                                        {{-- <td>
                                                                            {{ $pengajuan_beasiswa->bukti_pernah_sekolah }}
                                                                        </td> --}}
                                                                        {{-- <td>
                                                                            {{ $pengajuan_beasiswa->sktm }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->foto_rumah_depan }}
                                                                        </td> --}}
                                                                        {{-- <td>
                                                                            {{ $pengajuan_beasiswa->foto_ruang_tamu }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->foto_dapur }}
                                                                        </td> --}}
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->no_ktp }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->lokasi_pengajuan }}
                                                                        </td>

                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->tanggal_pengajuan }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->created_at }}
                                                                        </td>
                                                                        <td>
                                                                            <a href="#"
                                                                                class="btn btn-primary btn-action mr-1"
                                                                                data-toggle="tooltip" title="Edit"><i
                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                            <a href="#"
                                                                                class="btn btn-danger btn-action"
                                                                                onclick="event.preventDefault(); 
                                                                                document.getElementById('delete-form').submit();">
                                                                                <i class="fas fa-trash"></i>
                                                                            </a>
                                                                            <form id="delete-form"
                                                                                action="{{ route('pengajuan_beasiswa.delete', $pengajuan_beasiswa) }}"
                                                                                method="post" class="d-none">
                                                                                @csrf
                                                                                @method('delete')
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                    <tr>
                                                                        <td colspan="4" class="text-center">
                                                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                                                        </td>
                                                                    </tr>
                                                                @endforelse

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-pernyataan" role="tabpanel"
                                        aria-labelledby="pills-pernyataan-tab">
                                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h4>Latest Posts</h4>
                                                    <div class="card-header-action">
                                                        <a href="#" class="btn btn-primary">Edit</a>
                                                    </div>
                                                </div>
                                                <div class="card-body p-0">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped mb-0">
                                                            <thead>
                                                                <tr>
                                                                    <th>Nomor KTP :</th>
                                                                    <th>Tempat/ Kota mengisi Formulir :</th>
                                                                    <th>Tanggal mengisi Formulir</th>
                                                                    <th>Tanggal Diubah</th>
                                                                    <th style="min-width: 150px; text-align: center;">
                                                                        Action
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @forelse ($pengajuan_beasiswas as $pengajuan_beasiswa)
                                                                    <tr>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->no_ktp }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->lokasi_pengajuan }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->tanggal_pengajuan }}
                                                                        </td>
                                                                        <td>
                                                                            {{ $pengajuan_beasiswa->created_at }}
                                                                        </td>
                                                                        <td>
                                                                            <a href="#"
                                                                                class="btn btn-primary btn-action mr-1"
                                                                                data-toggle="tooltip" title="Edit"><i
                                                                                    class="fas fa-pencil-alt"></i></a>
                                                                            <a href="#"
                                                                                class="btn btn-danger btn-action"
                                                                                onclick="event.preventDefault(); 
                                                                                document.getElementById('delete-form').submit();">
                                                                                <i class="fas fa-trash"></i>
                                                                            </a>
                                                                            <form id="delete-form"
                                                                                action="{{ route('pengajuan_beasiswa.delete', $pengajuan_beasiswa) }}"
                                                                                method="post" class="d-none">
                                                                                @csrf
                                                                                @method('delete')
                                                                            </form>
                                                                        </td>
                                                                    </tr>
                                                                @empty
                                                                    <tr>
                                                                        <td colspan="4" class="text-center">
                                                                            <strong>DATA WAS NOT FOUND IN DATABASE</strong>
                                                                        </td>
                                                                    </tr>
                                                                @endforelse

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
