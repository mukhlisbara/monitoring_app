<?php


use App\Http\Controllers\AcademicController;
use App\Http\Controllers\ActivityPpaController;
use App\Http\Controllers\ActivityRegulerController;
use App\Http\Controllers\BeasiswaController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PengajuanBeasiswaController;
use App\Models\ActivityReguler;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/form', [HomeController::class, 'form'])->name('form');
Route::get('/activity_report_reguler', [HomeController::class, 'activity_report_reguler'])->name('activity_report_reguler');
Route::get('/activity_report_ppa', [HomeController::class, 'activity_report_ppa'])->name('activity_report_ppa');
Route::get('/academic_report', [HomeController::class, 'academic_report'])->name('academic_report');
Route::get('/pengajuan_beasiswa', [HomeController::class, 'pengajuan_beasiswa'])->name('pengajuan_beasiswa');
Route::get('/beasiswa_report', [HomeController::class, 'beasiswa_report'])->name('beasiswa_report');
Route::get('/beasiswa_report/edit/{report}', [BeasiswaController::class, 'edit'])->name('beasiswa_report.edit');
Route::get('/history_beasiswa_report', [HomeController::class, 'history_beasiswa_report'])->name('history_beasiswa_report');
Route::get('/history_activity_ppa', [HomeController::class, 'history_activity_ppa'])->name('history_activity_ppa');
Route::get('/history_activity_reguler', [HomeController::class, 'history_activity_reguler'])->name('history_activity_reguler');
Route::get('/history_academic_report', [HomeController::class, 'history_academic_report'])->name('history_academic_report');
Route::get('/history_pengajuan_beasiswa', [HomeController::class, 'history_pengajuan_beasiswa'])->name('history_pengajuan_beasiswa');

Route::get('/auth/google', [GoogleController::class, 'redirectToGoogle'])->name('google.login');
Route::get('/auth/google/callback', [GoogleController::class, 'handleGoogleCallback'])->name('google.callback');

// route::middleware(['auth', 'verified'])->group(function () {
//     Route::get('/dashboard', Dashboard::class)->name('dashboard');
// });

Route::middleware('auth')->group(function () {
    // Route::get('/fill-data/{new?}', FillData::class)->name('fill-data');
    // Route::get('/dashboard', Dashboard::class)->middleware(['auth', 'verified'])->name('dashboard');
    Route::get('/verify-edit', function () {
        return view('auth.verify-edit');
    });
});


// Laporan Beasiswa
Route::post('/store-beasiswa', [BeasiswaController::class, 'store'])->name('store-beasiswa');
Route::put('/update-beasiswa/{beasiswa}', [BeasiswaController::class, 'update'])->name('update-beasiswaReport');
Route::delete('/beasiswaReport/delete/{beasiswa}', [BeasiswaController::class, 'destroy'])->name('beasiswaReport.delete');

// Laporan Kegiatan PPA
Route::post('/store-activityPpa', [ActivityPpaController::class, 'store'])->name('store-activityPpa');
Route::put('/update-activityPpa/{activity_ppa}', [ActivityPpaController::class, 'update'])->name('update-activityPpa');
Route::delete('/activityPpa/delete/{activity_ppa}', [ActivityPpaController::class, 'destroy'])->name('activityPpa.delete');

// Laporan Kegiatan Reguler
Route::post('/store-activityReguler', [ActivityRegulerController::class, 'store'])->name('store-activityReguler');
Route::put('/update-activityReguler/{activityReguler}', [ActivityRegulerController::class, 'update'])->name('update-activityReguler');
Route::delete('/activityReguler/delete/{activity_reguler}', [ActivityRegulerController::class, 'destroy'])->name('activityReguler.delete');

// Laporan Akademik
Route::post('/store-academic', [AcademicController::class, 'store'])->name('store-academic');
Route::put('/update-academic/{academic}', [AcademicController::class, 'update'])->name('update-academic');
Route::delete('/academic/delete/{academic_report}', [AcademicController::class, 'destroy'])->name('academic.delete');

// Pengajuan Beasiswa
Route::post('/pengajuan-beasiswa', [PengajuanBeasiswaController::class, 'store'])->name('pengajuan_beasiswa.store');
Route::put('/pengajuan-beasiswa/{pengajuan}', [PengajuanBeasiswaController::class, 'update'])->name('pengajuan_beasiswa.update');
Route::delete('/pengajuan-beasiswa/delete/{pengajuan_beasiswa}', [PengajuanBeasiswaController::class, 'destroy'])->name('pengajuan_beasiswa.delete');
