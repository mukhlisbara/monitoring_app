<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('activity_ppas', function (Blueprint $table) {
            $table->id();
            $table->string('nama_anak_asuh');
            $table->string('nama_koordinator');
            $table->string('tingkat_sekolah');
            $table->string('kelas');
            $table->string('nama_surah');
            $table->string('jumlah_ayat');
            $table->string('keterangan');
            $table->string('link_youtube');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('activity_ppas');
    }
};
