<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('academics', function (Blueprint $table) {
            $table->id();
            $table->string('nama_anak_asuh');
            $table->string('no_telpon');
            $table->string('tingkat_sekolah');
            $table->string('kelas');
            $table->string('semester');
            $table->string('nilai_tertinggi');
            $table->string('nilai_terendah');
            $table->string('nilai_rata');
            $table->string('foto_rapot');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('academics');
    }
};
