<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('activity_regulers', function (Blueprint $table) {
            $table->id();
            $table->string('nama_anak_asuh');
            $table->string('nama_koordinator');
            $table->string('no_telpon');
            $table->string('tingkat_sekolah');
            $table->string('kelas');
            $table->string('sholat_5waktu');
            $table->string('sholat_dimasjid');
            $table->string('sholat_diawal');
            $table->string('sholat_sunnah_rawatib');
            $table->string('sholat_sunnah_tahiyatul');
            $table->string('sholat_sunnah_tahajud');
            $table->string('sholat_sunnah_dhuha');
            $table->string('sholat_sunnah_fajar');
            $table->string('sholat_sunnah_wudhu');
            $table->string('sholat_sunnah_hajad');
            $table->string('membaca_alquran');
            $table->string('hafalan_alquran');
            $table->string('puasa_ramadhan');
            $table->string('puasa_sunnah');
            $table->string('infaq_sedekah');
            $table->string('membantu_orangtua');
            $table->string('hubungan_ortu_wali');
            $table->string('belajar_mandiri');
            $table->string('belajar_kelompok');
            $table->string('membantu_teman');
            $table->string('mengaji_ustadz');
            $table->string('jumlah_hafalan');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('activity_regulers');
    }
};
