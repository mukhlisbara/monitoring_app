<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pengajuan_beasiswas', function (Blueprint $table) {
            $table->id();
            // $table->string('tipe_pendaftaran');
            $table->string('nama_koordinator');
            $table->string('nama_ortu');
            $table->string('ttl_ortu');
            $table->string('status_kawin');
            $table->string('pendidikan_terakhir');
            $table->string('kota');
            $table->string('alamat_detail');
            $table->string('pekerjaan');
            $table->string('pendapatan');
            $table->string('hak_kepemilikan_rumah');
            $table->string('jiwa_ditanggung');
            $table->string('siapa_ditanggung');
            $table->string('nama');
            $table->string('gender');
            $table->string('ttl');
            $table->string('status_anak_asuh');
            $table->string('tingkat_sekolah');
            $table->string('kelas_juli_ini');
            $table->string('nama_sekolah');
            $table->string('alamat_sekolah');
            $table->string('hubungan_wali_dengan_anak');
            $table->string('foto_anak_asuh');
            $table->string('ktp_ortu');
            $table->string('kk');
            $table->string('bukti_pernah_sekolah');
            $table->string('sktm');
            $table->string('foto_rumah_depan');
            $table->string('foto_ruang_tamu');
            $table->string('foto_dapur');
            $table->string('no_ktp');
            $table->string('lokasi_pengajuan');
            $table->string('tanggal_pengajuan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pengajuan_beasiswas');
    }
};
