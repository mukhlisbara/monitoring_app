<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class BeasiswaReport extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = ['nama_anak_asuh', 'nama_koordinator', 'foto_struk'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
