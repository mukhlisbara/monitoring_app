<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityPpa extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = ['nama_anak_asuh', 'nama_koordinator', 'tingkat_sekolah', 'kelas', 'nama_surah', 'jumlah_ayat', 'keterangan', 'link_youtube'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
