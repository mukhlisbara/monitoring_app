<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChildrenParent extends Model
{
    use HasFactory;
    protected $table = 'children_parents';
    protected $fillable = [
        'name',
        'birth_place',
        'birth_date',
        'marital',
        'tertiary_education',
        'address',
        'city',
        'job',
        'phone',
        'salary',
        'home_status',
        'number_of_souls',
        'category_of_souls',
    ];

    public function children()
    {
        return $this->hasMany('App\Models\child');
    }
}
