<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityReguler extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = [
        'nama_anak_asuh', 'nama_koordinator', 'no_telpon', 'tingkat_sekolah', 'kelas',
        'sholat_5waktu', 'sholat_dimasjid', 'sholat_diawal', 'sholat_sunnah_rawatib',
        'sholat_sunnah_tahiyatul', 'sholat_sunnah_tahajud', 'sholat_sunnah_dhuha',
        'sholat_sunnah_fajar', 'sholat_sunnah_wudhu', 'sholat_sunnah_hajad', 'membaca_alquran',
        'hafalan_alquran', 'puasa_ramadhan', 'puasa_sunnah', 'infaq_sedekah',
        'membantu_orangtua', 'hubungan_ortu_wali', 'belajar_mandiri', 'belajar_kelompok',
        'membantu_teman', 'mengaji_ustadz', 'jumlah_hafalan', 'keterangan',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
