<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Academic extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = ['nama_anak_asuh', 'no_telpon', 'tingkat_sekolah', 'kelas', 'semester', 'nilai_tertinggi', 'nilai_terendah', 'nilai_rata', 'foto_rapot', 'keterangan',];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
