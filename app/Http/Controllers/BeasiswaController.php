<?php

namespace App\Http\Controllers;

use App\Models\Beasiswa;
use App\Traits\UploadFile;
use App\Models\BeasiswaReport;
use Illuminate\Http\Request;


class BeasiswaController extends Controller
{
    use UploadFile;
    public function index()
    {
        return view('beasiswa_report');
    }

    public function create()
    {
        $beasiswa = BeasiswaReport::all();
        return view('beasiswa_report', compact('beasiswa'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_anak_asuh' => 'required',
            'nama_koordinator' => 'required',
            'foto_struk' => 'required',
        ]);
        // dd($request->foto_struk);

        $result = $this->_uploadFile($request->foto_struk, 'assets/img/struk');

        $beasiswa = new BeasiswaReport();
        if ($result['path']) {
            $beasiswa->foto_struk = $result['filename'];
        }

        $beasiswa->nama_anak_asuh  = $request->nama_anak_asuh;
        $beasiswa->nama_koordinator  = $request->nama_koordinator;

        $beasiswa->save();
        // BeasiswaReport::create($request->all());

        return redirect()->route('beasiswa_report')
            ->with('success', 'Beasiswa created successfully.');
    }

    public function edit(BeasiswaReport $report)
    {
        $nama_koordinator = ['Achmad Huzaini', 'Aestatica Ratri', 'Agus Indra Gunawan', 'Anjang S', 'Ardik Wijayanto', 'Dwi Gatot Saputro', 'Gatot Mardiyanto', "Hani'ah", 'Haryono', 'Heru Iswanto', 'Imam Nurul H', 'Irwan Sumarsono', 'Ismail', 'Johan Kuncoro', 'Lucky Pradipta', 'M Bagus S', 'M Chusnan', 'Marno', 'Moga Kurniajaya', 'Muh. Makhfut', 'Purwanto', 'Rengga Asmara', 'Rizal Mulia', 'Suprijanto', 'Suwaji', 'Tri Harsono', 'Warjilan'];
        return view('beasiswa_report-edit', compact(['report', 'nama_koordinator']));
    }

    public function update(Request $request, BeasiswaReport $beasiswa)
    {
        $request->validate([
            'nama_anak_asuh' => 'required',
            'nama_koordinator' => 'required',
            'foto_struk' => 'nullable',
        ]);


        try {
            $beasiswa->update($request->except('foto_struk'));
            // $beasiswa->fill($request->post())->save();

            if ($request->foto_struk) {
                $result = $this->_uploadFile($request->foto_struk, 'assets/img/struk');

                if ($result['path']) {
                    $beasiswa->foto_struk = $result['filename'];
                }
            }

            $beasiswa->save();
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Beasiswa has not been updated successfully');
        }

        return redirect()->route('history_beasiswa_report')
            ->with('success', 'Beasiswa updated successfully');

        // $beasiswa->save();

        // return back();
    }

    public function destroy(BeasiswaReport $beasiswa)
    {
        $beasiswa->delete();

        return redirect()->route('history_beasiswa_report')
            ->with('success', 'Beasiswa deleted successfully');
    }
}
