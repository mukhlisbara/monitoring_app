<?php

namespace App\Http\Controllers;

use App\Models\PengajuanBeasiswa;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

class PengajuanBeasiswaController extends Controller
{
    use UploadFile;
    public function store(Request $request)
    {
        // dd($request);
        $pengajuan = new PengajuanBeasiswa($request->except([
            'token',
            'foto_anak_asuh',
            'kk',
            'ktp_ortu',
            'bukti_pernah_sekolah',
            'sktm',
            'foto_rumah_depan',
            'foto_ruang_tamu',
            'foto_dapur'
        ]));

        $foto_anak_asuh = $this->_uploadFile($request->foto_anak_asuh, 'assets/img/pengajuan');

        if ($foto_anak_asuh['path']) {
            $pengajuan->foto_anak_asuh = $foto_anak_asuh['filename'];
        }

        $kk = $this->_uploadFile($request->kk, 'assets/img/pengajuan');

        if ($kk['path']) {
            $pengajuan->kk = $kk['filename'];
        }
        $ktp_ortu = $this->_uploadFile($request->ktp_ortu, 'assets/img/pengajuan');

        if ($ktp_ortu['path']) {
            $pengajuan->ktp_ortu = $ktp_ortu['filename'];
        }
        $bukti_pernah_sekolah = $this->_uploadFile($request->bukti_pernah_sekolah, 'assets/img/pengajuan');

        if ($bukti_pernah_sekolah['path']) {
            $pengajuan->bukti_pernah_sekolah = $bukti_pernah_sekolah['filename'];
        }
        $sktm = $this->_uploadFile($request->sktm, 'assets/img/pengajuan');

        if ($sktm['path']) {
            $pengajuan->sktm = $sktm['filename'];
        }
        $foto_rumah_depan = $this->_uploadFile($request->foto_rumah_depan, 'assets/img/pengajuan');

        if ($foto_rumah_depan['path']) {
            $pengajuan->foto_rumah_depan = $foto_rumah_depan['filename'];
        }
        $foto_ruang_tamu = $this->_uploadFile($request->foto_ruang_tamu, 'assets/img/pengajuan');

        if ($foto_ruang_tamu['path']) {
            $pengajuan->foto_ruang_tamu = $foto_ruang_tamu['filename'];
        }
        $foto_dapur = $this->_uploadFile($request->foto_dapur, 'assets/img/pengajuan');

        if ($foto_dapur['path']) {
            $pengajuan->foto_dapur = $foto_dapur['filename'];
        }

        $pengajuan->save();

        return back()->with('success', 'Pengajuan Beasiswa successfully');
    }

    public function destroy(PengajuanBeasiswa $pengajuan_beasiswa)
    {
        // dd($pengajuan_beasiswa);
        if (is_null($pengajuan_beasiswa)) {
            return redirect()->back()
                ->with('Error', 'Pengajuan deleted failed');
        }

        $pengajuan_beasiswa->delete();

        return redirect()->route('history_pengajuan_beasiswa')
            ->with('success', 'Pengajuan deleted successfully');
    }
}
