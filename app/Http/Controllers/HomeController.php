<?php

namespace App\Http\Controllers;

use App\Models\Academic;
use App\Models\ActivityPpa;
use App\Models\ActivityReguler;
use App\Models\Beasiswa;
use App\Models\BeasiswaReport;
use App\Models\PengajuanBeasiswa;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pengajuan_beasiswa = PengajuanBeasiswa::all();
        return view('home', ['pengajuan_beasiswas' => $pengajuan_beasiswa,]);
    }

    public function form()
    {
        return view('form');
    }
    public function activity_report_reguler()
    {
        $nama_koordinator = ['Achmad Huzaini', 'Aestatica Ratri', 'Agus Indra Gunawan', 'Anjang S', 'Ardik Wijayanto', 'Dwi Gatot Saputro', 'Gatot Mardiyanto', "Hani'ah", 'Haryono', 'Heru Iswanto', 'Imam Nurul H', 'Irwan Sumarsono', 'Ismail', 'Johan Kuncoro', 'Lucky Pradipta', 'M Bagus S', 'M Chusnan', 'Marno', 'Moga Kurniajaya', 'Muh. Makhfut', 'Purwanto', 'Rengga Asmara', 'Rizal Mulia', 'Suprijanto', 'Suwaji', 'Tri Harsono', 'Warjilan'];
        $tingkat_sekolah = ['TK', 'SD / MI', 'SMP / MTs', 'SMU / SMK / SMA'];
        $kelas = [
            'TK  kelas A', 'TK Kelas B', 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9', 'Kelas 10', 'Kelas 11', 'Kelas 12'
        ];
        $kegiatan = ['Selalu', 'Sering', 'Cukup', 'Jarang', 'Tidak Pernah'];

        return view('activity_report_reguler', [
            'nama_koordinator' => $nama_koordinator,
            'tingkat_sekolah' => $tingkat_sekolah,
            'kelas' => $kelas,
            'kegiatans' => $kegiatan,
        ]);
    }

    public function activity_report_ppa()
    {
        $nama_koordinator = ['Achmad Huzaini', 'Aestatica Ratri', 'Agus Indra Gunawan', 'Anjang S', 'Ardik Wijayanto', 'Dwi Gatot Saputro', 'Gatot Mardiyanto', "Hani'ah", 'Haryono', 'Heru Iswanto', 'Imam Nurul H', 'Irwan Sumarsono', 'Ismail', 'Johan Kuncoro', 'Lucky Pradipta', 'M Bagus S', 'M Chusnan', 'Marno', 'Moga Kurniajaya', 'Muh. Makhfut', 'Purwanto', 'Rengga Asmara', 'Rizal Mulia', 'Suprijanto', 'Suwaji', 'Tri Harsono', 'Warjilan'];
        $tingkat_sekolah = ['TK', 'SD / MI', 'SMP / MTs', 'SMU / SMK / SMA'];
        $kelas = [
            'TK  kelas A', 'TK Kelas B', 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9', 'Kelas 10', 'Kelas 11', 'Kelas 12'
        ];
        $keterangan = ['Sudah', 'Belum'];

        return view('activity_report_ppa', [
            'nama_koordinator' => $nama_koordinator,
            'tingkat_sekolah' => $tingkat_sekolah,
            'kelas' => $kelas,
            'keterangan' => $keterangan,
        ]);
    }

    public function academic_report()
    {
        // $nama_koordinator = ['Achmad Huzaini', 'Aestatica Ratri', 'Agus Indra Gunawan', 'Anjang S', 'Ardik Wijayanto', 'Dwi Gatot Saputro', 'Gatot Mardiyanto', "Hani'ah", 'Haryono', 'Heru Iswanto', 'Imam Nurul H', 'Irwan Sumarsono', 'Ismail', 'Johan Kuncoro', 'Lucky Pradipta', 'M Bagus S', 'M Chusnan', 'Marno', 'Moga Kurniajaya', 'Muh. Makhfut', 'Purwanto', 'Rengga Asmara', 'Rizal Mulia', 'Suprijanto', 'Suwaji', 'Tri Harsono', 'Warjilan'];
        $semester = ['Ganjil', 'Genap'];
        $tingkat_sekolah = ['TK', 'SD / MI', 'SMP / MTs', 'SMU / SMK / SMA'];
        $kelas = [
            'TK  kelas A', 'TK Kelas B', 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9', 'Kelas 10', 'Kelas 11', 'Kelas 12'
        ];
        $kegiatan = ['Selalu', 'Sering', 'Cukup', 'Jarang', 'Tidak Pernah'];

        return view('academic_report', [
            'semester' => $semester,
            'tingkat_sekolah' => $tingkat_sekolah,
            'kelas' => $kelas,
            'kegiatans' => $kegiatan,
        ]);
    }

    public function pengajuan_beasiswa()
    {
        $nama_koordinator = ['Achmad Huzaini', 'Aestatica Ratri', 'Agus Indra Gunawan', 'Anjang S', 'Ardik Wijayanto', 'Dwi Gatot Saputro', 'Gatot Mardiyanto', "Hani'ah", 'Haryono', 'Heru Iswanto', 'Imam Nurul H', 'Irwan Sumarsono', 'Ismail', 'Johan Kuncoro', 'Lucky Pradipta', 'M Bagus S', 'M Chusnan', 'Marno', 'Moga Kurniajaya', 'Muh. Makhfut', 'Purwanto', 'Rengga Asmara', 'Rizal Mulia', 'Suprijanto', 'Suwaji', 'Tri Harsono', 'Warjilan'];
        $beasiswa = ['Beasiswa Reguler (Daftar Ulang)', 'Beasiswa Reguler (Daftar Baru)', "Beasiswa Penghafal Al Qur'an (Daftar Ulang)", "Beasiswa Penghafal Al Qur'an (Daftar Baru)"];
        $kelamin = ['Laki-laki', 'Perempuan'];
        $tingkat_sekolah = ['TK', 'SD / MI', 'SMP / MTs', 'SMU / SMK / SMA'];
        $kelas = [
            'TK  kelas A', 'TK Kelas B', 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9', 'Kelas 10', 'Kelas 11', 'Kelas 12'
        ];

        return view('pengajuan_beasiswa', [
            'nama_koordinator' => $nama_koordinator,
            'beasiswa' => $beasiswa,
            'kelamin' => $kelamin,
            'tingkat_sekolah' => $tingkat_sekolah,
            'kelas' => $kelas,
        ]);
    }

    public function beasiswa_report()
    {
        $nama_koordinator = ['Achmad Huzaini', 'Aestatica Ratri', 'Agus Indra Gunawan', 'Anjang S', 'Ardik Wijayanto', 'Dwi Gatot Saputro', 'Gatot Mardiyanto', "Hani'ah", 'Haryono', 'Heru Iswanto', 'Imam Nurul H', 'Irwan Sumarsono', 'Ismail', 'Johan Kuncoro', 'Lucky Pradipta', 'M Bagus S', 'M Chusnan', 'Marno', 'Moga Kurniajaya', 'Muh. Makhfut', 'Purwanto', 'Rengga Asmara', 'Rizal Mulia', 'Suprijanto', 'Suwaji', 'Tri Harsono', 'Warjilan'];

        return view('beasiswa_report', [
            'nama_koordinator' => $nama_koordinator,
        ]);
    }

    public function history_beasiswa_report()
    {

        $nama_koordinator = ['Achmad Huzaini', 'Aestatica Ratri', 'Agus Indra Gunawan', 'Anjang S', 'Ardik Wijayanto', 'Dwi Gatot Saputro', 'Gatot Mardiyanto', "Hani'ah", 'Haryono', 'Heru Iswanto', 'Imam Nurul H', 'Irwan Sumarsono', 'Ismail', 'Johan Kuncoro', 'Lucky Pradipta', 'M Bagus S', 'M Chusnan', 'Marno', 'Moga Kurniajaya', 'Muh. Makhfut', 'Purwanto', 'Rengga Asmara', 'Rizal Mulia', 'Suprijanto', 'Suwaji', 'Tri Harsono', 'Warjilan'];
        $nama_anak_asuh = ['Hendra', 'Joni'];

        $beasiswa = BeasiswaReport::all();

        return view('history_beasiswa_report', [
            'nama_koordinator' => $nama_koordinator,
            'nama_anak_asuh' => $nama_anak_asuh,
            'beasiswas' => $beasiswa
        ]);
    }

    public function history_activity_ppa()
    {
        $activity_ppa = ActivityPpa::all();

        return view('history_activity_ppa', [
            'activity_ppa' => $activity_ppa
        ]);
    }

    public function history_activity_reguler()
    {
        $activity_reguler = ActivityReguler::all();

        return view('history_activity_reguler', [
            'activity_reguler' => $activity_reguler
        ]);
    }

    public function history_academic_report()
    {
        $academic_report = Academic::all();

        return view('history_academic_report', [
            'academic_report' => $academic_report
        ]);
    }

    public function history_pengajuan_beasiswa()
    {
        $pengajuan_beasiswa = PengajuanBeasiswa::all();
        $beasiswa = ['Beasiswa Reguler (Daftar Ulang)', 'Beasiswa Reguler (Daftar Baru)', "Beasiswa Penghafal Al Qur'an (Daftar Ulang)", "Beasiswa Penghafal Al Qur'an (Daftar Baru)"];
        $nama_koordinator = ['Achmad Huzaini', 'Aestatica Ratri', 'Agus Indra Gunawan', 'Anjang S', 'Ardik Wijayanto', 'Dwi Gatot Saputro', 'Gatot Mardiyanto', "Hani'ah", 'Haryono', 'Heru Iswanto', 'Imam Nurul H', 'Irwan Sumarsono', 'Ismail', 'Johan Kuncoro', 'Lucky Pradipta', 'M Bagus S', 'M Chusnan', 'Marno', 'Moga Kurniajaya', 'Muh. Makhfut', 'Purwanto', 'Rengga Asmara', 'Rizal Mulia', 'Suprijanto', 'Suwaji', 'Tri Harsono', 'Warjilan'];
        $beasiswa = ['Beasiswa Reguler (Daftar Ulang)', 'Beasiswa Reguler (Daftar Baru)', "Beasiswa Penghafal Al Qur'an (Daftar Ulang)", "Beasiswa Penghafal Al Qur'an (Daftar Baru)"];
        $kelamin = ['Laki-laki', 'Perempuan'];
        $tingkat_sekolah = ['TK', 'SD / MI', 'SMP / MTs', 'SMU / SMK / SMA'];
        $kelas = [
            'TK  kelas A', 'TK Kelas B', 'Kelas 1', 'Kelas 2', 'Kelas 3', 'Kelas 4', 'Kelas 5', 'Kelas 6', 'Kelas 7', 'Kelas 8', 'Kelas 9', 'Kelas 10', 'Kelas 11', 'Kelas 12'
        ];

        return view('history_pengajuan_beasiswa', [
            'pengajuan_beasiswas' => $pengajuan_beasiswa,
            'beasiswa' => $beasiswa,
            'nama_koordinator' => $nama_koordinator,
            'kelamin' => $kelamin,
            'tingkat_sekolah' => $tingkat_sekolah,
            'kelas' => $kelas,
        ]);
    }
}
