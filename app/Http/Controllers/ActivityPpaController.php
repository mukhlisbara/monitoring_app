<?php

namespace App\Http\Controllers;

use App\Models\ActivityPpa;
use Illuminate\Http\Request;

class ActivityPpaController extends Controller
{
    public function store(Request $request)
    {
        try {
            $ppa = new ActivityPpa($request->except('token'));

            if ($ppa->save()) {
                return redirect()->route('activity_report_ppa')->with('success', 'new Activity created successfully.');
            }
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->route('activity_report_ppa')->with('error', 'Sorry, the Activity was not saved.');
        }
    }

    public function destroy(ActivityPpa $activity_ppa)
    {
        // dd($activity_ppa);
        if (is_null($activity_ppa)) {
            return redirect()->back()
                ->with('Error', 'Activity deleted successfully');
        }

        $activity_ppa->delete();

        return redirect()->route('history_activity_ppa')
            ->with('success', 'Activity deleted successfully');
    }
}
