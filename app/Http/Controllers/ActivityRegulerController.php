<?php

namespace App\Http\Controllers;

use App\Models\ActivityReguler;
use Illuminate\Http\Request;

class ActivityRegulerController extends Controller
{
    public function store(Request $request)
    {
        try {
            $reguler = new ActivityReguler($request->except('token'));

            if ($reguler->save()) {
                return redirect()->route('activity_report_reguler')->with('success', 'new Activity created successfully.');
            }
        } catch (\Throwable $th) {
            //throw $th;
            return redirect()->route('activity_report_reguler')->with('error', 'Sorry, the Activity was not saved.');
        }
    }

    public function destroy(ActivityReguler $activity_reguler)
    {
        // dd($activity_reguler);
        if (is_null($activity_reguler)) {
            return redirect()->back()
                ->with('Error', 'Activity deleted failed');
        }

        $activity_reguler->delete();

        return redirect()->route('history_activity_reguler')
            ->with('success', 'Activity deleted successfully');
    }
}
