<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // public function login(Request $request)
    // {
    //     $validated = $request->validate([
    //         'email' => ['required', 'email:rfc,dns'],
    //         'password' => ['required'],
    //     ]);

    //     if (Auth::attempt(['email' => $validated['email'], 'password' => $validated['password']])) {
    //         // if (Auth::user()->id_role === 1) {
    //         //     // return route('superadmin.home');
    //         //     // dd('test');
    //         //     return redirect()->route('superadmin.home');
    //         // } else if (Auth::user()->id_role === 2) {
    //         //     return redirect()->route('admin.home');
    //         // } else {
    //         //     return redirect()->route('user.home');
    //         // }
    //     } else {
    //         return redirect()->route('login')->withErrors('Email atau Password salah');
    //     }
    // }
}
