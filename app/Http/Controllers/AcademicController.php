<?php

namespace App\Http\Controllers;

use App\Models\Academic;
use App\Traits\UploadFile;
use Illuminate\Http\Request;

class AcademicController extends Controller
{
    use UploadFile;
    public function store(Request $request)
    {
        $request->validate([
            // 'nama_anak_asuh' => ,
            'nama_anak_asuh' => 'sometimes|required',
            'no_telpon' => 'sometimes|required',
            'tingkat_sekolah' => 'sometimes|required',
            'kelas' => 'sometimes|required',
            'semester' => 'sometimes|required',
            'nilai_tertinggi' => 'sometimes|required',
            'nilai_terendah' => 'sometimes|required',
            'nilai_rata' => 'sometimes|required',
            'keterangan' => 'sometimes|required',
            'foto_rapot' => ['required', 'image'],


        ]);
        // dd($request->foto_rapot);

        // Upload Foto
        $result = $this->_uploadFile($request->foto_rapot, 'assets/img/rapot');

        $academic_report = new Academic($request->except('token'));
        if ($result['path']) {
            $academic_report->foto_rapot = $result['filename'];
        } else {
            return redirect()->route('academic_report')->with('error', 'Sorry, the Activity was not saved.');
        }

        if ($academic_report->save()) {
            return redirect()->route('academic_report')->with('success', 'Academic created successfully.');
        }

        //throw $th;
        // return redirect()->route('academic_report')->with('error', 'Sorry, the Activity was not saved.');

        // try {
        //     $academic_report = new Academic($request->except('token'));

        //     if ($academic_report->save()) {
        //         return redirect()->route('academic_report')->with('success', 'Academic created successfully.');
        //     }
        // } catch (\Throwable $th) {
        //     //throw $th;
        //     return redirect()->route('academic_report')->with('error', 'Sorry, the Activity was not saved.');
        // }
    }

    public function destroy(Academic $academic_report)
    {
        // dd($academic_report);
        $academic_report->delete();

        return redirect()->route('history_academic_report')
            ->with('success', 'Academic deleted successfully');
    }
}
